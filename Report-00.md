### 1. Git là gì và làm được gì :
- Git là tên gọi của một trong những hệ thống quản lý phiên bản phân tán (DVCS) phổ biến nhất hiện nay.
- Git giúp lưu lại các phiên bản của những lần thay đổi vào mã nguồn và có thể khôi phục lại dễ dàng mà không cần copy lại mã nguồn rồi cất vào đâu đó. 1 người khác có thể xem các thay đổi đó ở từng phiên bản, cũng có thể đối chiếu các thay đổi đó rồi gộp phiên bản của họ vào.
- Git hỗ trợ Teamwork tốt bởi cơ chế phân tán (khả năng tạo nhánh )
### 2. Các khái niệm cơ bản :
- **Repository** : Là nơi chứa tất cả những thông tin cần thiết để duy trì và quản lý các sửa đổi và lịch sử của toàn bộ project. Có 2 loại Remote Repo để chia sẻ giữa nhiều người và bố trí trên server chuyên dụng ; Local Repo bố trí trên máy của bản thân dành cho mình sử dụng. Có 2 kiểu cấu trúc dự liệu trong Repo là Object store và Index. 
- **Object Store** : Chứa dữ liệu nguyên gốc, các file log ghi chép quá trình sửa đổi, tên người tạo file, ngày tháng và các thông tin khác. 4 object cơ bản bao gồm : tree - tương đương 1 directory ; blob - tương đương 1 file ; commit ; tag .
- **Index** : Là file nhị phân động và tạm thời miêu tả cấu trú thư mục của toàn bộ Repo và trạng thái của Project được thể hiện thông qua commit và tree tại một thời điểm nào đó trong lịch sử phát triển.
- **Branch** : Là cái dùng để phân nhánh và ghi lại luồng của lịch sử. Có thể thay đổi đồng thời code trong các nhánh mà không làm ảnh hưởng đến nhánh chính.
- **Staging Area** : Là khu vực lưu trữ những thay đổi trên tập tin để nó có thể commit, đối với git, muốn commit tập tin nào thì tập tin ấy phải có trong Staging Area.
### 3. Git & Github & Gitlab :
- Github & Gitlab là một dịch vụ máy chủ Repo công cộng, mỗi người có thể tạo tài khoản trên đó để tạo ra các kho chứa của riêng mình để có thể làm việc
- Github là github.com thuộc quyền của Microsoft còn Gitlab là gitlab.com của thằng nào đó nhưng có thể 1 công ty tự tải về và setup mà sử dụng nội bộ.
### 4. Các trạng thái có thể có trong Git Repo :
- **Untracked** : Các tập tin mới được tạo ra trong working space ở trạng thái Untracked có thể hiểu là chưa được Git biết tới.
- **Unmodified** : Khi file vừa được commit thì sẽ chuyển tới trạng thái này.
- **Modified** : Nếu 1 file trong Repo được sửa sau khi commit sẽ chuyển tới trạng thái này.
- **Staged** : File ở trạng thái này sẽ sẵn sàng để commit.

