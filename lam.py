import flask
from flask import request, jsonify
import argparse
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, verify_jwt_in_request, get_jwt_claims
import datetime
from functools import wraps
import sqlite3

app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this!
jwt = JWTManager(app)

parser = argparse.ArgumentParser(description='path file')
parser.add_argument('path', help='path file', nargs='?', default='data/users.json')  #default path='data/users.json
data = parser.parse_args()                                                           #ex: python user.py path="data/data/json"
fname = data.path

@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400
    with open(fname, 'r') as f:
        data = json.load(f)
        i=0
        while data[i]['name'] != username:
            i+=1
            if i == len(data):
                return jsonify({"msg": "Invalid username"}), 401

    if password != data[i]['password']:
        return jsonify({"msg": "Invalid password"}), 401

    # Identity can be any data that is json serializable
    expires = datetime.timedelta(days=365)
    access_token = create_access_token(identity=username, expires_delta=expires)
    return jsonify(access_token=access_token), 200

def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        print(type(claims))
        if claims['roles'] != 'admin':
            return jsonify(msg='Admins only!'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper


@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    if identity == 'admin':
        return {'roles': 'admin'}
    else:
        return {'roles': 'peasant'}
# List all users
@app.route('/users', methods=['GET'])
@admin_required
def get_users():
    try:
        filesize = os.path.getsize(fname)
        with open(fname) as file:
            if filesize == 0:
                return "Data empty", 404
            else:
                msg = json.load(file)
    except FileNotFoundError as err:
        return jsonify("File path not exist in server!"), 500

    return jsonify(msg)

# Get one user by id from the path
@app.route("/user/<id>", methods=['GET'])
# @jwt_required
def get_user_by_id_in_path(id):
    try:
        conn = sqlite3.connect(path_db)
    except sqlite3.Error as e:
        return str(e), 500
    try:
        c = conn.cursor()
        c.execute('SELECT id, user, age FROM users WHERE id = ?', (int(id),))
        data = c.fetchone()
        user = {"id": data[0], "user": data[1], "age": data[2]}
        conn.commit()
        conn.close()
        return jsonify(user), 200
    except:
        return 'ID is not exists', 404


# Add new user
@app.route('/user', methods=['POST'])
@jwt_required
def post_users():
    try:
        # new_user = request.get_json()  # Neu khong yeu cau format
        #yeu cau format
        new_user = {
            "name": request.json["name"],
            "age": request.json["age"],
            "id": request.json["id"]
        }
    except:
        return jsonify("Body format json and only include name, age and id"), 400
    try:
        new = []
        filesize = os.path.getsize(fname)
        if not os.path.isfile(fname) or filesize == 0:    #create file if file empty
            new.append(new_user)
            with open(fname, mode='w') as file:
                json.dump(new, file)
        else:
            with open(fname, "r+") as file:
                data = json.load(file)
                data.append(new_user)
                file.seek(0)
                json.dump(data, file)
    except FileNotFoundError:
        return("File path not exist in server"), 500

    return jsonify({"new user":new_user}), 201


# Update user
@app.route('/user', methods=['PUT'])
# @jwt_required
def put_users():
    try:
        uid= flask.request.json['id']
        new_username= flask.request.json['user']
        new_age = flask.request.json['age']
        new_password= flask.request.json['password']
    except:
        return 'New data must be in the right format: {"id": your_id, "user": "new_username", "password": "new_password", "age": new_age}', 400
    try:
        conn = sqlite3.connect(path_db)
    except sqlite3.Error as e:
        return str(e), 500
    try:
        c = conn.cursor()
        c.execute('UPDATE users SET user=?, password=?, age=? WHERE id=?', (new_username, new_password, new_age, uid))
        conn.commit()
        conn.close()
        return "Update user successfully !!!"
    except:
        return "User is not exists", 404

# Delete user by id
@app.route('/user/<id>', methods=['DELETE'])
@jwt_required
def delete_users(id):
    try:
        filesize = os.path.getsize(fname)
        with open(fname, "r+") as file:
            if filesize == 0:                        # ckeck xem file empty khong
                return("Data not found"), 404
            else:
                data = json.load(file)
                for user in data:
                    if user['id'] == int(id):
                        data.remove(user)
                        file.truncate(0)  #clear data in file
                        file.seek(0)      #đưa con trỏ về đầu file
                        json.dump(data, file)
                    return jsonify({"deleted user":user}), 200
        return("ID not match"), 400
    except FileNotFoundError:
        return ("File path not exist in server"), 500


app.run(debug=True, host='0.0.0.0')
# curl -H "Content-Type: application/json"  -XPOST -d '{"name":"Adam","age":14,"id":1}' http://localhost:5000/user
# curl -H "Content-Type: application/json"  -XPUT -d '[{"name":"Eva","age":14,"id":1}]' http://localhost:5000/user
# curl -X DELETE http://localhost:5000/user/1
