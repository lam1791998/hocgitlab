import flask
from flask import request, jsonify
import argparse
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, verify_jwt_in_request, get_jwt_claims
import datetime
from functools import wraps
import sqlite3
from flask_sqlalchemy import SQLAlchemy


app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this!
app.config['JSON_SORT_KEYS'] = False
jwt = JWTManager(app)

# Truyền đường dẫn database từ dòng lệnh , ex: python user.py "data/user.db"
parser = argparse.ArgumentParser(description='path file')
parser.add_argument('path', help='path file', nargs='?', default='/home/intern/api.db')
data = parser.parse_args()
path_db = data.path

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + str(path_db)

db = SQLAlchemy(app)

class users(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user = db.Column(db.String(12), unique=True, nullable=False)
    password = db.Column(db.String(12), nullable=False)
    age = db.Column(db.Integer)
    role = db.Column(db.String(12), server_default='viewer')

try:
    db.create_all()
except:

# nancy = users(user='nanc2', password='123', age=18)
# db.session.add(nancy)
# db.session.commit()
# Check user/pass trước khi call API
@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400
    conn_sql = sqlite3.connect(path_db)
    user_pass = conn_sql.execute("SELECT user, password,role from users")
    for check in user_pass:
        if (username, password) == check[0:2]:
            expires = datetime.timedelta(days=365)
            identity = {
                'user': username,
                'role': check[2]
            }
            access_token = create_access_token(identity=identity, expires_delta=expires)
            return jsonify(access_token=access_token), 200
    return jsonify({"msg": "Invalid username or password"}), 401

# decoder user
@jwt.user_identity_loader
def user_identity_lookup(identity):
    return identity['user']
# decoder role của user
@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    return {'role': identity['role']}

# Check role của user (access nếu role == admin)
def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()

        if claims['role'] != 'admin':
            return jsonify(msg='Admins only!'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper

#List all users dưới dạng json (required role = admin)
@app.route('/users', methods=['GET'])
@admin_required
def get_users():
    try:
        conn_sql = sqlite3.connect(path_db)
        cursor = conn_sql.execute("SELECT * from users")
        users = []
        for row in cursor:
            a = {"ID": row[0], "user": row[1], "password": row[2], "age": row[3], "role": row[4]}
            users.append(a)
        conn_sql.close()
        if len(users) == 0:
            return jsonify("Data empty"), 404
    except :
        return jsonify("Unable to open database file"), 500
    return jsonify(users), 200

# Get one user by id from the path
@app.route("/user/<id>", methods=['GET'])
@jwt_required
def get_user_by_id_in_path(id):
    user = users.query.get(id)
    if user is None:
        return 'ID is not exists', 404
    # data = users.query.get(id),
    data = {
        "id": user.id,
        "user": user.user,
        "age": user.age,
        "role": user.role
    }
    return jsonify(data)


# Add new user
@app.route('/user', methods=['POST'])
@jwt_required
def post_users():
    try:
        new_user = {
            "user": request.json["user"],
            "password": request.json["password"],
            "age": request.json["age"]
        }
        info_new_user = (new_user["user"], new_user["password"], new_user["age"])
    except :
        return jsonify("Body format json and only include user, password and age "), 400
    try:
        conn_sql = sqlite3.connect(path_db)
        conn_sql.execute("INSERT INTO users(user, password, age) VALUES " + str(info_new_user))
        conn_sql.commit()
        conn_sql.close()
    except sqlite3.IntegrityError:
        return("Username already exists. Please try another one "), 500
    except:
        return jsonify("Unable to open database file"), 500

    return jsonify({"new user": new_user}), 201


# Update user
@app.route('/user', methods=['PUT'])
@jwt_required
def put_users():
    try:
        username = flask.request.json['user']
        new_age = flask.request.json['age']
        new_password = flask.request.json['password']
    except:
        return 'New data must be in the right format: {"user": "your_username", "password": "new_password", "age": new_age}', 400
    if (type(new_age) != int) or (new_age < 18):
        return 'Age must be an INTEGER and 18+', 400
    if ((type(username) or type(new_password)) != str) or ((len(username) or len(new_password)) > 12):
        return 'Username/password must be a STRING and could not have more than 12 characters', 400

    user = users.query.filter_by(user=username)
    if user is None:
        return 'User is not exists', 404
    for i in user:
        i.password = new_password
        i.age = new_age
    db.session.commit()
    return "Update user successfully"
    # try:
    #     conn = sqlite3.connect(path_db)
    # except sqlite3.Error as e:
    #     return str(e), 500
    # try:
    #     c = conn.cursor()
    #     c.execute('UPDATE users password=?, age=? WHERE user =?', (new_password, new_age, username))
    #     conn.commit()
    #     conn.close()
    #     return "Update user successfully !!!"
    # except:
    #     return "User is not exists", 404

# Delete user by id
@app.route('/user/<id>', methods=['DELETE'])
@jwt_required
def delete_users(id):
    try:
        conn_sql = sqlite3.connect(path_db)
        check_id = conn_sql.execute("SELECT ID from users")
        for i in check_id:
            if int(i[0]) == int(id):
                user = conn_sql.execute("SELECT  user from users where ID =" + str(id))
                conn_sql.execute("DELETE from users where ID =" + str(id))
                conn_sql.commit()
                user_del = [u[0] for u in user]
                conn_sql.close()
                return jsonify({"del":{"id": id, "user": user_del[0]}}), 200
        return "ID not match", 400
    except :
        return ("Unable to open database file"), 500


app.run(debug=True, host='0.0.0.0')
# curl -H "Content-Type: application/json"  -XPOST -d '{"name":"Adam","age":14,"id":1}' http://localhost:5000/user
# curl -H "Content-Type: application/json"  -XPUT -d '[{"name":"Eva","age":14,"id":1}]' http://localhost:5000/user
# curl -X DELETE http://localhost:5000/user/1

